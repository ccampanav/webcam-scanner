﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class GenPasswd
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(GenPasswd))
        Me.Password = New System.Windows.Forms.PictureBox()
        Me.Apply = New System.Windows.Forms.Label()
        Me.TBPasswd = New System.Windows.Forms.TextBox()
        CType(Me.Password, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Password
        '
        Me.Password.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Password.Location = New System.Drawing.Point(12, 62)
        Me.Password.Name = "Password"
        Me.Password.Size = New System.Drawing.Size(340, 180)
        Me.Password.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.Password.TabIndex = 1
        Me.Password.TabStop = False
        '
        'Apply
        '
        Me.Apply.AutoSize = True
        Me.Apply.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Apply.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Apply.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Apply.Location = New System.Drawing.Point(253, 18)
        Me.Apply.Name = "Apply"
        Me.Apply.Size = New System.Drawing.Size(88, 22)
        Me.Apply.TabIndex = 6
        Me.Apply.Text = "Generar"
        '
        'TBPasswd
        '
        Me.TBPasswd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBPasswd.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBPasswd.ForeColor = System.Drawing.Color.DimGray
        Me.TBPasswd.Location = New System.Drawing.Point(25, 14)
        Me.TBPasswd.MaxLength = 12
        Me.TBPasswd.Name = "TBPasswd"
        Me.TBPasswd.Size = New System.Drawing.Size(220, 31)
        Me.TBPasswd.TabIndex = 5
        Me.TBPasswd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GenPasswd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(364, 261)
        Me.Controls.Add(Me.Apply)
        Me.Controls.Add(Me.TBPasswd)
        Me.Controls.Add(Me.Password)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximumSize = New System.Drawing.Size(380, 300)
        Me.MinimumSize = New System.Drawing.Size(380, 300)
        Me.Name = "GenPasswd"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Webcam Scanner"
        CType(Me.Password, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Password As PictureBox
    Friend WithEvents Apply As Label
    Friend WithEvents TBPasswd As TextBox
End Class
