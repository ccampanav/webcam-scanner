﻿Public Class GenPasswd
    Dim canClick As Boolean = False

    Private Sub GenPasswd_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Apply.Select()
    End Sub

    Private Sub Apply_Click(sender As Object, e As EventArgs) Handles Apply.Click
        If Len(TBPasswd.Text) >= 4 And Len(TBPasswd.Text) <= 12 Then
            Dim sizeW As UShort = 0 : Dim strFinal As String = "0"
            For i = 1 To Len(TBPasswd.Text) Step 1
                sizeW += Mid(TBPasswd.Text, i, 1) : strFinal = strFinal & Mid(TBPasswd.Text, i, 1) & "0"
            Next
            Call DrawCode((((Len(TBPasswd.Text) + 1) * 2) + sizeW), strFinal)
        Else
            MsgBox("La contraseña debe tener de 4 a 12 dígitos.", MsgBoxStyle.Information, "Webcam Scanner")
        End If
    End Sub

    Private Sub DrawCode(ByVal sizeW As UShort, ByVal code As String)
        sizeW = (sizeW * 10) : Dim sizeH As UShort = (sizeW / 2)
        Dim codeImg As Bitmap = New Bitmap(sizeW, sizeH)
        For y = 0 To sizeH - 1 Step 1
            For x = 0 To sizeW - 1 Step 1
                If x = 0 Or x = sizeW - 1 Then
                    codeImg.SetPixel(x, y, Color.Red)
                Else
                    codeImg.SetPixel(x, y, Color.White)
                End If
            Next
        Next
        Dim sentinel As UShort = 0 : Dim n As UShort
        For i = 1 To Len(code) Step 1
            n = Mid(code, i, 1)
            If n = "0" Then
                n += 20
            Else
                n = n * 10 - 1
                For h = sentinel To sentinel + n Step 1
                    For y = 0 To sizeH - 1 Step 1
                        codeImg.SetPixel(h, y, Color.Black)
                    Next
                Next
            End If
            sentinel += n
        Next
        codeImg.Save(My.Application.Info.DirectoryPath & "\passwd.png", Imaging.ImageFormat.Png)
        Password.Image = codeImg
    End Sub

    Private Sub Password_Click(sender As Object, e As EventArgs) Handles Password.Click
        Try
            Process.Start(My.Application.Info.DirectoryPath & "\passwd.png")
        Catch ex As Exception
        End Try
    End Sub

    Private Sub TBPasswd_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TBPasswd.KeyPress
        If InStr(Home.TBFilter, e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub GenPasswd_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If My.Computer.FileSystem.FileExists("passwd.png") Then
            My.Computer.FileSystem.DeleteFile("passwd.png")
        End If
    End Sub

End Class