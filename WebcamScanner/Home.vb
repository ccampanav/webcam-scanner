﻿Imports AForge.Video
Imports AForge.Video.DirectShow

Public Class Home
    Dim s_unlock As Media.SoundPlayer
    Dim sov As VideoCaptureDevice : Dim sel_wc As FilterInfo : Dim devices As FilterInfoCollection
    Dim imgSOV As Bitmap : Dim resX, resY As UShort : Dim detecting As Boolean = False
    Dim rangeRGB As Byte = 120 : Public TBFilter As String = "1234" : Dim passwdFound As Boolean
    Dim lines As UShort : Dim minW, maxW As Byte : Dim tole As Byte = 2
    Dim passwd2Found As String : Dim passwd(11) As UShort : Dim lenPasswd As Byte
    Dim detect, contLines As UShort : Dim strLines As String : Dim maxMultiply As Byte = 10 : Dim nowMultiply As Byte
    Dim movePos, nowMove, n, cont, mintole, maxtole As UShort : Dim go As Byte

    Private Sub Home_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        s_unlock = New Media.SoundPlayer("sounds/unlocked.wav")
        resX = 640 : resY = 480 : minW = 4 : maxW = 100 : CheckForIllegalCrossThreadCalls = False
        devices = New FilterInfoCollection(FilterCategory.VideoInputDevice) : Webcam.Select()
        If devices.Count = 0 Then
            TBPasswd.Enabled = False : Apply.Enabled = False
            MsgBox("No hay Webcams disponibles.", MsgBoxStyle.Information, "Webcam Scanner")
        Else
            sel_wc = devices.Item(0) : sov = New VideoCaptureDevice(sel_wc.MonikerString)
            AddHandler sov.NewFrame, New NewFrameEventHandler(AddressOf Video_NewFrame) : sov.Start()
        End If
    End Sub

    Private Sub Video_NewFrame(sender As Object, eventArgs As AForge.Video.NewFrameEventArgs)
        Try
            imgSOV = DirectCast(eventArgs.Frame.Clone(), Bitmap)
            If detecting = True Then
                lines = 0 : strLines = "" : contLines = 0
                For x = 0 To 639 Step 1
                    detect = 0
                    For y = 239 To 241 Step 1 'Media: 240
                        Dim clr As Color = imgSOV.GetPixel(x, y)
                        If clr.R < rangeRGB And clr.G < rangeRGB And clr.B < rangeRGB Then
                            imgSOV.SetPixel(x, y, Color.Lime)
                        Else
                            imgSOV.SetPixel(x, y, Color.White) : detect += 1
                        End If
                    Next
                    If detect = 0 Then
                        contLines += 1
                    Else
                        If contLines >= minW And contLines <= maxW Then
                            strLines = strLines & contLines & "-" : contLines = 0
                        End If
                    End If
                Next
                strLines = Microsoft.VisualBasic.Left(strLines, Len(strLines) - 1)
                Dim arrayLines() As String = Split(strLines, "-") : lines = arrayLines.Length
                If lines >= lenPasswd Then
                    nowMultiply = 1 : movePos = (lines - lenPasswd) : nowMove = 0 : passwdFound = False
                    While nowMultiply <= maxMultiply
                        For i = 0 To lenPasswd Step 1
                            n = Mid(passwd2Found, i + 1, 1) : passwd(i) = n * nowMultiply
                        Next
                        While nowMove < movePos
                            cont = 0 : go = 0
                            For i = nowMove To nowMove + lenPasswd Step 1
                                If passwd(cont) > tole Then
                                    mintole = passwd(cont) - tole
                                Else
                                    mintole = 1
                                End If
                                maxtole = passwd(cont) + tole
                                If arrayLines(i) >= mintole And arrayLines(i) <= maxtole Then
                                    go += 1
                                End If
                                cont += 1
                            Next
                            If go = lenPasswd + 1 Then
                                passwdFound = True : Exit While
                            End If
                            nowMove += 1
                        End While
                        If passwdFound = True Then
                            Exit While
                        End If
                        nowMove = 0 : nowMultiply += 1
                    End While
                End If
                If passwdFound = True Then
                    detecting = False : PBDetect.Image = My.Resources.btn_off : s_unlock.Play()
                    MsgBox("Sistema desbloqueado.", MsgBoxStyle.Information, "Webcam Scanner") : End
                End If
            Else
                lines = 0
            End If
            Webcam.Image = imgSOV
        Catch ex As Exception
        End Try
    End Sub

    Private Sub Apply_Click(sender As Object, e As EventArgs) Handles Apply.Click
        If Len(TBPasswd.Text) >= 4 And Len(TBPasswd.Text) <= 12 Then
            passwd2Found = TBPasswd.Text : lenPasswd = Len(passwd2Found) - 1
            TBPasswd.Enabled = False : Apply.Enabled = False : detecting = True : PBDetect.Image = My.Resources.btn_on
        Else
            MsgBox("La contraseña debe tener de 4 a 12 dígitos.", MsgBoxStyle.Information, "Webcam Scanner")
        End If
    End Sub

    Private Sub GenPsswd_Click(sender As Object, e As EventArgs) Handles GenPsswd.Click
        GenPasswd.Show()
    End Sub

    Private Sub TBPasswd_KeyPress(sender As Object, e As KeyPressEventArgs) Handles TBPasswd.KeyPress
        If InStr(TBFilter, e.KeyChar) Then
            e.Handled = False
        ElseIf Char.IsControl(e.KeyChar) Then
            e.Handled = False
        Else
            e.Handled = True
        End If
    End Sub

    Private Sub TBRangeRGB_Scroll(sender As Object, e As EventArgs) Handles TBRangeRGB.Scroll
        rangeRGB = TBRangeRGB.Value : LabelRangeRGB.Text = rangeRGB
    End Sub

    Private Sub Home_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            sov.SignalToStop() : Webcam.Image = Nothing
        Catch ex As Exception
        End Try
    End Sub

End Class
