﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Home
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Home))
        Me.GenPsswd = New System.Windows.Forms.Label()
        Me.TBPasswd = New System.Windows.Forms.TextBox()
        Me.Apply = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TBRangeRGB = New System.Windows.Forms.TrackBar()
        Me.LabelRangeRGB = New System.Windows.Forms.Label()
        Me.PBDetect = New System.Windows.Forms.PictureBox()
        Me.Webcam = New System.Windows.Forms.PictureBox()
        CType(Me.TBRangeRGB, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBDetect, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Webcam, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GenPsswd
        '
        Me.GenPsswd.AutoSize = True
        Me.GenPsswd.Cursor = System.Windows.Forms.Cursors.Hand
        Me.GenPsswd.Font = New System.Drawing.Font("Century Gothic", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GenPsswd.ForeColor = System.Drawing.Color.DodgerBlue
        Me.GenPsswd.Location = New System.Drawing.Point(538, 40)
        Me.GenPsswd.Name = "GenPsswd"
        Me.GenPsswd.Size = New System.Drawing.Size(199, 22)
        Me.GenPsswd.TabIndex = 1
        Me.GenPsswd.Text = "Generar contraseña"
        '
        'TBPasswd
        '
        Me.TBPasswd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBPasswd.Font = New System.Drawing.Font("Century Gothic", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TBPasswd.ForeColor = System.Drawing.Color.DimGray
        Me.TBPasswd.Location = New System.Drawing.Point(528, 136)
        Me.TBPasswd.MaxLength = 12
        Me.TBPasswd.Name = "TBPasswd"
        Me.TBPasswd.Size = New System.Drawing.Size(220, 28)
        Me.TBPasswd.TabIndex = 3
        Me.TBPasswd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Apply
        '
        Me.Apply.AutoSize = True
        Me.Apply.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Apply.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Apply.ForeColor = System.Drawing.Color.DodgerBlue
        Me.Apply.Location = New System.Drawing.Point(606, 172)
        Me.Apply.Name = "Apply"
        Me.Apply.Size = New System.Drawing.Size(66, 21)
        Me.Apply.TabIndex = 4
        Me.Apply.Text = "Aplicar"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(556, 315)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 20)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "Detectando:"
        '
        'TBRangeRGB
        '
        Me.TBRangeRGB.AutoSize = False
        Me.TBRangeRGB.Location = New System.Drawing.Point(88, 394)
        Me.TBRangeRGB.Maximum = 220
        Me.TBRangeRGB.Minimum = 20
        Me.TBRangeRGB.Name = "TBRangeRGB"
        Me.TBRangeRGB.Size = New System.Drawing.Size(350, 25)
        Me.TBRangeRGB.TabIndex = 9
        Me.TBRangeRGB.TickStyle = System.Windows.Forms.TickStyle.None
        Me.TBRangeRGB.Value = 120
        '
        'LabelRangeRGB
        '
        Me.LabelRangeRGB.AutoSize = True
        Me.LabelRangeRGB.Font = New System.Drawing.Font("Century Gothic", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelRangeRGB.ForeColor = System.Drawing.Color.DodgerBlue
        Me.LabelRangeRGB.Location = New System.Drawing.Point(443, 394)
        Me.LabelRangeRGB.Name = "LabelRangeRGB"
        Me.LabelRangeRGB.Size = New System.Drawing.Size(37, 21)
        Me.LabelRangeRGB.TabIndex = 10
        Me.LabelRangeRGB.Text = "120"
        '
        'PBDetect
        '
        Me.PBDetect.Cursor = System.Windows.Forms.Cursors.Default
        Me.PBDetect.Image = Global.WebcamScanner.My.Resources.Resources.btn_off
        Me.PBDetect.Location = New System.Drawing.Point(663, 312)
        Me.PBDetect.Name = "PBDetect"
        Me.PBDetect.Size = New System.Drawing.Size(50, 25)
        Me.PBDetect.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBDetect.TabIndex = 7
        Me.PBDetect.TabStop = False
        '
        'Webcam
        '
        Me.Webcam.BackColor = System.Drawing.Color.White
        Me.Webcam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Webcam.Cursor = System.Windows.Forms.Cursors.Default
        Me.Webcam.Location = New System.Drawing.Point(12, 12)
        Me.Webcam.Name = "Webcam"
        Me.Webcam.Size = New System.Drawing.Size(500, 375)
        Me.Webcam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Webcam.TabIndex = 0
        Me.Webcam.TabStop = False
        '
        'Home
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(764, 421)
        Me.Controls.Add(Me.LabelRangeRGB)
        Me.Controls.Add(Me.TBRangeRGB)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PBDetect)
        Me.Controls.Add(Me.Apply)
        Me.Controls.Add(Me.TBPasswd)
        Me.Controls.Add(Me.GenPsswd)
        Me.Controls.Add(Me.Webcam)
        Me.Font = New System.Drawing.Font("Century Gothic", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.DimGray
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.MaximumSize = New System.Drawing.Size(780, 460)
        Me.MinimumSize = New System.Drawing.Size(780, 460)
        Me.Name = "Home"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Webcam Scanner"
        CType(Me.TBRangeRGB, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBDetect, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Webcam, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Webcam As PictureBox
    Friend WithEvents GenPsswd As Label
    Friend WithEvents TBPasswd As TextBox
    Friend WithEvents Apply As Label
    Friend WithEvents PBDetect As PictureBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TBRangeRGB As TrackBar
    Friend WithEvents LabelRangeRGB As Label
End Class
