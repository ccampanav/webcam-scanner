Released at: June 2016

Description: Generate a numeric password, print or download the password in barcode (passwd.png). Finally write the password and apply it for read it when you show it in front of webcam.
